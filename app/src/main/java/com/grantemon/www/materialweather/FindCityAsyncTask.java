package com.grantemon.www.materialweather;

import android.os.AsyncTask;
import android.widget.TextView;

import com.grantemon.www.materialweather.models.City;
import com.grantemon.www.materialweather.models.WeatherConsts;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import static com.grantemon.www.materialweather.Load.requestExecute;

public class FindCityAsyncTask extends AsyncTask<String, Void, City> {
    TextView cityNameSearchResult;
    TextView cityIdSearchResult;

    public FindCityAsyncTask(TextView cityOutputName,TextView cityOutputId) {
        cityNameSearchResult = cityOutputName;
        cityIdSearchResult   = cityOutputId;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected City doInBackground(String... params) {
        String correctCityName;
        String correctCityInformation;
        City city = new City(WeatherConsts.EMPTY_ID,WeatherConsts.EMPTY_CITY_NAME);

        try {
            correctCityName = findCorrectCityName(requestExecute(WeatherConsts.FIND_REQUEST, params[0]));
            correctCityInformation = requestExecute(WeatherConsts.FIND_REQUEST, correctCityName);

            city = findCity(correctCityInformation);

        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }

        return city;
    }

    @Override
    protected void onPostExecute(City city) {
        super.onPostExecute(city);
        cityIdSearchResult.setText(String.valueOf(city.getId()));
        cityNameSearchResult.setText(city.getName());

    }

    private String findCorrectCityName(String cityInformation) throws IOException, JSONException {
        String cityName = "";
        JSONObject jsonObjectWithCityInformation;
        JSONArray arrayWithCityInformation;
        if (cityInformation != null) {
            JSONObject findRequestJsonObj = new JSONObject(cityInformation);
            arrayWithCityInformation = findRequestJsonObj.getJSONArray("list");
            jsonObjectWithCityInformation = arrayWithCityInformation.getJSONObject(0);
            cityName = jsonObjectWithCityInformation.getString("name");
        }
        return cityName;
    }

    private City findCity(String cityInformation) throws JSONException, IOException {
        int cityId;
        String cityName;
        City city = null;

        JSONObject jsonObjectWithCityInformation;
        JSONArray arrayWithCityInformation;

        if (cityInformation != null) {
            JSONObject findRequestJsonObj = new JSONObject(cityInformation);

            if("accurate".equals(findRequestJsonObj.getString("message"))){
                arrayWithCityInformation      = findRequestJsonObj.getJSONArray("list");
                jsonObjectWithCityInformation = arrayWithCityInformation.getJSONObject(0);

                cityId   = jsonObjectWithCityInformation.getInt("id");
                cityName = jsonObjectWithCityInformation.getString("name");

                city = new City(cityId,cityName);
            } else {
                throw new  IOException();
            }


        }
        return city;
    }
}