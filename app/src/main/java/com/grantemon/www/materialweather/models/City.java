package com.grantemon.www.materialweather.models;

public class City {
    private int id;
    private String name;
    private int active;

    public City(int id, String name, int active) {
        this.id = id;
        this.name = name;
        this.active = active;
    }
    public City(int id, String name) {
        this.id = id;
        this.name = name;
        this.active = WeatherConsts.FALSE;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }
}
