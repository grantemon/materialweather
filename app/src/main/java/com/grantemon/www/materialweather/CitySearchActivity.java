package com.grantemon.www.materialweather;

import android.content.ContentValues;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.grantemon.www.materialweather.models.WeatherConsts;

import static com.grantemon.www.materialweather.models.WeatherConsts.TAG;

public class CitySearchActivity extends AppCompatActivity {

    EditText cityNameInputForSearch;
    TextView cityNameSearchResult;
    TextView cityIdSearchResult;
    private DatabaseHelper mDatabaseHelper;
    //todo take to come method
    SharedPreferences sPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city_search);

        cityNameInputForSearch = (EditText) findViewById(R.id.cityNameInputForSearch);
        cityNameSearchResult = (TextView) findViewById(R.id.cityNameSearchResult);
        cityIdSearchResult = (TextView) findViewById(R.id.cityIdSearchResult);

        cityNameInputForSearch.addTextChangedListener(new TextWatcher() {
            String city = "";

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {
                    city = String.valueOf(cityNameInputForSearch.getText());
                    new Load(cityNameInputForSearch, cityNameSearchResult, cityIdSearchResult, city, WeatherConsts.FIND_REQUEST);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void onClick(View view) {

        switch (view.getId()){
            case R.id.cityNameSearchResult:
                mDatabaseHelper = new DatabaseHelper(this,DatabaseHelper.DATABASE_NAME, null, DatabaseHelper.DATABASE_VERSION);
                SQLiteDatabase sdb;
                sdb = mDatabaseHelper.getReadableDatabase();

                sPref = getSharedPreferences(WeatherConsts.PREFERENCES_FILE, MODE_PRIVATE);
                //todo realize what to do if 0 return
                int activeCity = sPref.getInt(WeatherConsts.PREFERANCES_ACTIVE_CITY, 0);

                Log.d(TAG, "activeCity" + activeCity);

                ContentValues values = new ContentValues();
                values.put(DatabaseHelper.CITY_ID_COLUMN, Integer.parseInt(String.valueOf(cityIdSearchResult.getText())));
                values.put(DatabaseHelper.CITY_NAME_COLUMN, String.valueOf(cityNameSearchResult.getText()));
                values.put(DatabaseHelper.ACTIVE, WeatherConsts.TRUE);
                sdb.update(DatabaseHelper.CITIES_TABLE, values, "id = ?", new String[]{
                        String.valueOf(activeCity)
                });

                Cursor cursor = sdb.query(DatabaseHelper.CITIES_TABLE, new String[]{DatabaseHelper.ID_COLUMN,
                                DatabaseHelper.CITY_ID_COLUMN,DatabaseHelper.CITY_NAME_COLUMN,DatabaseHelper.ACTIVE},
                        "active = ?", new String[]{
                                String.valueOf(WeatherConsts.TRUE)
                        },
                        null, null, null) ;


                Log.d(TAG, "перед нахождением");
                //cursor.moveToFirst();
                cursor.move(activeCity);
                String name = "name: ";
                int id = 50;
                int sid = 0;
                name += cursor.getString(cursor.getColumnIndex(DatabaseHelper.CITY_NAME_COLUMN));
                id = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.CITY_ID_COLUMN));
                Log.d(TAG,"до того как все тронул");
                sid = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.ID_COLUMN));
                Log.d(TAG,name + id + ":pos " + sid);

                cursor.close();
                break;
            case R.id.searchCityBackButton:
                this.finish();
                break;
        }




    }
}
