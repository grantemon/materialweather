package com.grantemon.www.materialweather;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;

import com.grantemon.www.materialweather.models.Weather;
import com.grantemon.www.materialweather.models.WeatherConsts;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import static com.grantemon.www.materialweather.Load.requestExecute;

public class Load5DayAsyncTask extends AsyncTask <Void, Void, Weather[]>{
    Context context;
    int activeCity;
    SQLiteDatabase sdb;

    public Load5DayAsyncTask(Context context) {
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        SharedPreferences sPref;
        sPref = context.getSharedPreferences(WeatherConsts.PREFERENCES_FILE, Context.MODE_PRIVATE);
        activeCity = sPref.getInt(WeatherConsts.PREFERANCES_ACTIVE_CITY, 0);

        DatabaseHelper mDatabaseHelper = new DatabaseHelper(context,DatabaseHelper.DATABASE_NAME, null, DatabaseHelper.DATABASE_VERSION);
        sdb = mDatabaseHelper.getReadableDatabase();
    }

    @Override
    protected Weather[] doInBackground(Void... params) {
        Weather [] weathers = new Weather[0];

        Cursor cursor = sdb.query(DatabaseHelper.CITIES_TABLE, new String[]{DatabaseHelper.ID_COLUMN,
                        DatabaseHelper.CITY_ID_COLUMN,DatabaseHelper.CITY_NAME_COLUMN,DatabaseHelper.ACTIVE},
                null, null,
                null, null, null) ;

        cursor.move(activeCity);
        String cityName = cursor.getString(cursor.getColumnIndex(DatabaseHelper.CITY_NAME_COLUMN));
        String result;
        try {
            result = requestExecute(WeatherConsts.FIVE_DAY_WEATHER_REQUEST, cityName);
            weathers = fiveСurrentWeather(result);
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        cursor.close();
        return weathers;
    }

    @Override
    protected void onPostExecute(Weather... weathers) {
        super.onPostExecute(weathers);

        int positionToInput = WeatherConsts.FIVE_DAY_WEATHER_START;

        for (Weather weather : weathers) {
            ContentValues values = new ContentValues();

            values.put(DatabaseHelper.WEATHER_TEMPERATURE_COLUMN, weather.getTemperature());
            values.put(DatabaseHelper.WEATHER_HUMIDITY_COLUMN, weather.getHumidity());
            values.put(DatabaseHelper.WEATHER_WIND_SPEED_COLUMN, weather.getWindSpeed());
            values.put(DatabaseHelper.WEATHER_WIND_DEGREE_COLUMN, weather.getWindDirection());
            values.put(DatabaseHelper.WEATHER_WEATHER_ID_COLUMN, weather.getWeatherId());
            values.put(DatabaseHelper.WEATHER_CITY_ID_COLUMN, weather.getCityId());
            values.put(DatabaseHelper.WEATHER_UNIX_TIME_COLUMN, weather.getUnixTime());
            sdb.update(DatabaseHelper.WEATHERS_TABLE, values, "id = ?", new String[]{
                    String.valueOf(positionToInput++)
            });
            Log.d(WeatherConsts.TAG, weather.getTemperature() + ":" + weather.getUnixTime());
        }

        //todo check functionality
        ContentValues emptyWeatherValues = new ContentValues();
        emptyWeatherValues.put(DatabaseHelper.WEATHER_TEMPERATURE_COLUMN,WeatherConsts.ZERO);
        emptyWeatherValues.put(DatabaseHelper.WEATHER_HUMIDITY_COLUMN,WeatherConsts.ZERO);
        emptyWeatherValues.put(DatabaseHelper.WEATHER_WIND_SPEED_COLUMN,WeatherConsts.ZERO);
        emptyWeatherValues.put(DatabaseHelper.WEATHER_WIND_DEGREE_COLUMN,WeatherConsts.ZERO);
        emptyWeatherValues.put(DatabaseHelper.WEATHER_WEATHER_ID_COLUMN,WeatherConsts.ZERO);
        emptyWeatherValues.put(DatabaseHelper.WEATHER_CITY_ID_COLUMN,WeatherConsts.ZERO);
        emptyWeatherValues.put(DatabaseHelper.WEATHER_UNIX_TIME_COLUMN,WeatherConsts.ZERO);

        for (int i = positionToInput; i <= WeatherConsts.FIVE_DAY_WEATHER_END; i++) {
            sdb.update(DatabaseHelper.WEATHERS_TABLE, emptyWeatherValues, "id = ?", new String[]{
                    String.valueOf(i)
            });
        }
    }


    private Weather[] fiveСurrentWeather(String currentWeatherInformation) throws JSONException, IOException {

        Weather [] weathers = new Weather[0];

        JSONObject jsonObjectWithWeatherSpecifications;
        JSONArray  arrayWithWeatherInformation;

        double temperature;
        int    humidity;
        double windSpeed;
        double windDegree;
        int    weatherId;
        int    cityId;
        long   unixTime;

        int    count;

        if (currentWeatherInformation != null) {
            JSONObject currentWeatherRequestJsonObj = new JSONObject(currentWeatherInformation);
            JSONObject cityJsonObject = currentWeatherRequestJsonObj.getJSONObject("city");
            cityId     = cityJsonObject.getInt("id");
            count = currentWeatherRequestJsonObj.getInt("cnt");
            weathers = new Weather[count];

            JSONObject weatherInformationJsonObject;

            arrayWithWeatherInformation   = currentWeatherRequestJsonObj.getJSONArray("list");
            for (int i = 0; i < count; i++) {
                weatherInformationJsonObject = arrayWithWeatherInformation.getJSONObject(i);

                jsonObjectWithWeatherSpecifications = weatherInformationJsonObject.getJSONObject("main");
                temperature  = jsonObjectWithWeatherSpecifications.getDouble("temp");
                humidity = jsonObjectWithWeatherSpecifications.getInt("humidity");
                jsonObjectWithWeatherSpecifications = weatherInformationJsonObject.getJSONObject("wind");
                windSpeed  = jsonObjectWithWeatherSpecifications.getDouble("speed");
                windDegree = jsonObjectWithWeatherSpecifications.getDouble("deg");
                weatherId  = weatherInformationJsonObject.getJSONArray("weather").getJSONObject(WeatherConsts.ZERO).getInt("id");
                unixTime   = weatherInformationJsonObject.getLong("dt");

                weathers[i] = new Weather(i,temperature,humidity,windSpeed,windDegree,weatherId,cityId,unixTime);
            }
        }
        return weathers;
    }
}
