package com.grantemon.www.materialweather;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.grantemon.www.materialweather.adapters.PerHourWeatherAdapter;
import com.grantemon.www.materialweather.models.FiveDayWeatherForAdapter;
import com.grantemon.www.materialweather.models.Weather;
import com.grantemon.www.materialweather.models.WeatherConsts;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class PageFragment extends Fragment {

    static final String ARGUMENT_PAGE_NUMBER = "arg_page_number";
    static final int CITY_PAGE             = 0;
    static final int CURRENT_WEATHER_PAGE  = 1;
    static final int PER_HOUR_WEATHER_PAGE = 2;
    static final int FIVE_DAY_WEATHER_PAGE = 3;

    int pageNumber;
    int backColor;
    int activeCity;
    static Context context;
    SharedPreferences sPref;
    String ActiveCityName;

    public static void setContext(Context context) {
        PageFragment.context = context;
    }

    static PageFragment newInstance(int page) {
        PageFragment pageFragment = new PageFragment();
        Bundle arguments = new Bundle();
        arguments.putInt(ARGUMENT_PAGE_NUMBER, page);
        pageFragment.setArguments(arguments);
        return pageFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Log.d(WeatherConsts.TAG, "on create page fragment");
        pageNumber = getArguments().getInt(ARGUMENT_PAGE_NUMBER);

        Random rnd = new Random();
        backColor = Color.argb(40, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //Log.d(WeatherConsts.TAG, "before reading prefs");
        sPref = context.getSharedPreferences(WeatherConsts.PREFERENCES_FILE, Context.MODE_PRIVATE);
        activeCity = sPref.getInt(WeatherConsts.PREFERANCES_ACTIVE_CITY, 0);


        //Log.v(WeatherConsts.TAG,"active city in crv" + activeCity);
        View view = null;
        switch (pageNumber){
            case CITY_PAGE:
                Log.v(WeatherConsts.TAG,"CITY_PAGE");
                view = inflater.inflate(R.layout.city_page_fragment, null);
                fillCitiesPage(view);
                break;
            case CURRENT_WEATHER_PAGE:
                Log.v(WeatherConsts.TAG,"CURRENT_WEATHER_PAGE");
                view = inflater.inflate(R.layout.current_weather_page_fragment, null);
                if (WeatherConsts.checkInternetConnection(context)){
                    new LoadCurrentAsyncTask(context, view).execute();
                }
                fillCurrentWeatherPage(view);
                break;
            case PER_HOUR_WEATHER_PAGE:
                Log.v(WeatherConsts.TAG,"PER_HOUR_WEATHER_PAGE");
                view = inflater.inflate(R.layout.per_hour_weather_page, null);
                if (WeatherConsts.checkInternetConnection(context)){
                    new Load5DayAsyncTask(context).execute();
                }
                Log.v(WeatherConsts.TAG,"перед адаптером");
                fillPerHourWeatherPage(view);
                Log.v(WeatherConsts.TAG, "после адаптера");
                break;
            case FIVE_DAY_WEATHER_PAGE:
                Log.v(WeatherConsts.TAG,"FIVE_DAY_WEATHER_PAGE");
                view = inflater.inflate(R.layout.five_day_weather_page, null);
                break;
        }

        return view;
    }

    private void fillCitiesPage(View cityPage){
        Button firstCityButton  = (Button) cityPage.findViewById(R.id.firstCityButton);
        Button secondCityButton = (Button) cityPage.findViewById(R.id.secondCityButton);
        Button thirdCityButton  = (Button) cityPage.findViewById(R.id.thirdCityButton);
        Button fourCityButton   = (Button) cityPage.findViewById(R.id.fourCityButton);


        if (activeCity != 0) {
            DatabaseHelper mDatabaseHelper;
            mDatabaseHelper = new DatabaseHelper(context,DatabaseHelper.DATABASE_NAME, null, DatabaseHelper.DATABASE_VERSION);
            SQLiteDatabase sdb;
            sdb = mDatabaseHelper.getReadableDatabase();

            Cursor cursor = sdb.query(DatabaseHelper.CITIES_TABLE, new String[]{DatabaseHelper.ID_COLUMN,
                            DatabaseHelper.CITY_ID_COLUMN,DatabaseHelper.CITY_NAME_COLUMN,DatabaseHelper.ACTIVE},
                    null, null,
                    null, null, null) ;

            cursor.moveToFirst();
            if(cursor.getInt(cursor.getColumnIndex(DatabaseHelper.CITY_ID_COLUMN)) != 0){
                firstCityButton.setText(cursor.getString(cursor.getColumnIndex(DatabaseHelper.CITY_NAME_COLUMN)));
            }

            cursor.moveToNext();
            if(cursor.getInt(cursor.getColumnIndex(DatabaseHelper.CITY_ID_COLUMN)) != 0){
                secondCityButton.setText(cursor.getString(cursor.getColumnIndex(DatabaseHelper.CITY_NAME_COLUMN)));
            }

            cursor.moveToNext();
            if(cursor.getInt(cursor.getColumnIndex(DatabaseHelper.CITY_ID_COLUMN)) != 0){
                thirdCityButton.setText(cursor.getString(cursor.getColumnIndex(DatabaseHelper.CITY_NAME_COLUMN)));
            }

            cursor.moveToNext();
            if(cursor.getInt(cursor.getColumnIndex(DatabaseHelper.CITY_ID_COLUMN)) != 0){
                fourCityButton.setText(cursor.getString(cursor.getColumnIndex(DatabaseHelper.CITY_NAME_COLUMN)));
            }

            cursor.close();

            cursor = sdb.query(DatabaseHelper.CITIES_TABLE, new String[]{DatabaseHelper.ID_COLUMN,
                            DatabaseHelper.CITY_ID_COLUMN,DatabaseHelper.CITY_NAME_COLUMN,DatabaseHelper.ACTIVE},
                    "active = ?", new String[]{
                            String.valueOf(WeatherConsts.TRUE)
                    },
                    null, null, null) ;

            cursor.moveToFirst();
            ActiveCityName = String.valueOf(cursor.getInt(cursor.getColumnIndex(DatabaseHelper.CITY_NAME_COLUMN)));

            cursor.close();
        }

    }
    private void fillCurrentWeatherPage(View currentWeatherPage){
        TextView cityName                    = (TextView) currentWeatherPage.findViewById(R.id.cityName);
        TextView currentWeatherTemperature   = (TextView) currentWeatherPage.findViewById(R.id.currentWeatherTemperature);
        TextView currentWeatherName          = (TextView) currentWeatherPage.findViewById(R.id.currentWeatherName);
        TextView currentWeatherHumidity      = (TextView) currentWeatherPage.findViewById(R.id.currentWeatherHumidity);
        TextView currentWeatherWindSpeed     = (TextView) currentWeatherPage.findViewById(R.id.currentWeatherWindSpeed);
        TextView currentWeatherWindDirection = (TextView) currentWeatherPage.findViewById(R.id.currentWeatherWindDirection);


        if (activeCity != 0) {
            DatabaseHelper mDatabaseHelper;
            mDatabaseHelper = new DatabaseHelper(context,DatabaseHelper.DATABASE_NAME, null, DatabaseHelper.DATABASE_VERSION);
            SQLiteDatabase sdb;
            sdb = mDatabaseHelper.getReadableDatabase();

            Cursor cursor = sdb.query(DatabaseHelper.WEATHERS_TABLE, new String[]{
                            DatabaseHelper.WEATHER_ID_COLUMN,
                            DatabaseHelper.WEATHER_TEMPERATURE_COLUMN,
                            DatabaseHelper.WEATHER_HUMIDITY_COLUMN,
                            DatabaseHelper.WEATHER_WIND_SPEED_COLUMN,
                            DatabaseHelper.WEATHER_WIND_DEGREE_COLUMN,
                            DatabaseHelper.WEATHER_WEATHER_ID_COLUMN,
                            DatabaseHelper.WEATHER_CITY_ID_COLUMN,
                            DatabaseHelper.WEATHER_UNIX_TIME_COLUMN
                    },
                    null, null,
                    null, null, null) ;

            cursor.moveToFirst();

            //if city present
            if(cursor.getInt(cursor.getColumnIndex(DatabaseHelper.CITY_ID_COLUMN)) != 0){
                String currentWeatherTemperatureString   = String.valueOf(WeatherConsts.getCelsiusTemperature(cursor.getDouble(cursor.getColumnIndex(DatabaseHelper.WEATHER_TEMPERATURE_COLUMN))) + " C");
                String currentWeatherNameString          = WeatherConsts.getWeatherById(cursor.getInt(cursor.getColumnIndex(DatabaseHelper.WEATHER_WEATHER_ID_COLUMN)));
                String currentWeatherHumidityString      = String.valueOf("Влажность " + cursor.getInt(cursor.getColumnIndex(DatabaseHelper.WEATHER_HUMIDITY_COLUMN)));
                String currentWeatherWindSpeedString     = String.valueOf("Скорость ветра " + cursor.getDouble(cursor.getColumnIndex(DatabaseHelper.WEATHER_WIND_SPEED_COLUMN)));
                String currentWeatherWindDirectionString = String.valueOf("Направление ветра " + cursor.getDouble(cursor.getColumnIndex(DatabaseHelper.WEATHER_WIND_DEGREE_COLUMN)));
                String currentWeatherUnixTime            = String.valueOf(cursor.getLong(cursor.getColumnIndex(DatabaseHelper.WEATHER_UNIX_TIME_COLUMN))*1000);

                cityName.setText(String.format("%s %s", this.ActiveCityName, new Date()));
                currentWeatherTemperature.setText(currentWeatherTemperatureString);
                currentWeatherName.setText(currentWeatherNameString);
                currentWeatherHumidity.setText(currentWeatherHumidityString);
                currentWeatherWindSpeed.setText(currentWeatherWindSpeedString);
                currentWeatherWindDirection.setText(currentWeatherWindDirectionString);
            }
            cursor.close();
        }

    }


    private void fillPerHourWeatherPage(View perHourWeatherPage){

        ListView perHourWeatherListView = (ListView) perHourWeatherPage.findViewById(R.id.perHourWeatherListView);
        ArrayList<Weather> weathers = new ArrayList<Weather>();



        DatabaseHelper mDatabaseHelper;
        mDatabaseHelper = new DatabaseHelper(context,DatabaseHelper.DATABASE_NAME, null, DatabaseHelper.DATABASE_VERSION);
        SQLiteDatabase sdb;
        sdb = mDatabaseHelper.getReadableDatabase();

        Cursor cursor = sdb.query(DatabaseHelper.WEATHERS_TABLE, new String[]{
                        DatabaseHelper.WEATHER_ID_COLUMN,
                        DatabaseHelper.WEATHER_TEMPERATURE_COLUMN,
                        DatabaseHelper.WEATHER_HUMIDITY_COLUMN,
                        DatabaseHelper.WEATHER_WIND_SPEED_COLUMN,
                        DatabaseHelper.WEATHER_WIND_DEGREE_COLUMN,
                        DatabaseHelper.WEATHER_WEATHER_ID_COLUMN,
                        DatabaseHelper.WEATHER_CITY_ID_COLUMN,
                        DatabaseHelper.WEATHER_UNIX_TIME_COLUMN
                },
                null, null,
                null, null, null) ;

        cursor.moveToFirst();

        for (int i = 0; i < 8; i++) {

            cursor.moveToNext();
            int    id = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.WEATHER_ID_COLUMN));
            double temperature = cursor.getDouble(cursor.getColumnIndex(DatabaseHelper.WEATHER_TEMPERATURE_COLUMN));
            int    humidity = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.WEATHER_HUMIDITY_COLUMN));
            double windSpeed = cursor.getDouble(cursor.getColumnIndex(DatabaseHelper.WEATHER_WIND_SPEED_COLUMN));
            double windDirection = cursor.getDouble(cursor.getColumnIndex(DatabaseHelper.WEATHER_WIND_DEGREE_COLUMN));
            int    weatherId = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.WEATHER_WEATHER_ID_COLUMN));
            int    cityId = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.WEATHER_CITY_ID_COLUMN));
            long   unixTime = cursor.getLong(cursor.getColumnIndex(DatabaseHelper.WEATHER_UNIX_TIME_COLUMN));

            weathers.add(new Weather(id, temperature, humidity, windSpeed, windDirection, weatherId, cityId, unixTime));
        }


        cursor.close();

        PerHourWeatherAdapter phwa = new PerHourWeatherAdapter(context, weathers);

        perHourWeatherListView.setAdapter(phwa);

    }

    private void fillFiveDayWeatherPage(View fiveDayWeatherPage){
        ListView fiveDayWeatherListView = (ListView) fiveDayWeatherPage.findViewById(R.id.fiveDayWeatherListView);
        ArrayList<FiveDayWeatherForAdapter> weathers = new ArrayList<FiveDayWeatherForAdapter>();




        DatabaseHelper mDatabaseHelper;
        mDatabaseHelper = new DatabaseHelper(context,DatabaseHelper.DATABASE_NAME, null, DatabaseHelper.DATABASE_VERSION);
        SQLiteDatabase sdb;
        sdb = mDatabaseHelper.getReadableDatabase();

        Cursor cursor = sdb.query(DatabaseHelper.WEATHERS_TABLE, new String[]{
                        DatabaseHelper.WEATHER_ID_COLUMN,
                        DatabaseHelper.WEATHER_TEMPERATURE_COLUMN,
                        DatabaseHelper.WEATHER_HUMIDITY_COLUMN,
                        DatabaseHelper.WEATHER_WIND_SPEED_COLUMN,
                        DatabaseHelper.WEATHER_WIND_DEGREE_COLUMN,
                        DatabaseHelper.WEATHER_WEATHER_ID_COLUMN,
                        DatabaseHelper.WEATHER_CITY_ID_COLUMN,
                        DatabaseHelper.WEATHER_UNIX_TIME_COLUMN
                },
                null, null,
                null, null, null) ;

        cursor.moveToFirst();
        Date d = new Date();
        d.getDay();

        while (cursor.isAfterLast()){
            cursor.moveToNext();
            long   unixTime = cursor.getLong(cursor.getColumnIndex(DatabaseHelper.WEATHER_UNIX_TIME_COLUMN))*1000;
            Map<Integer,Integer> currentFiveDaysFrom= new HashMap<Integer,Integer>();


        }
       /* for (int i = 0; i < 8; i++) {

            cursor.moveToNext();
            int    id = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.WEATHER_ID_COLUMN));
            double temperature = cursor.getDouble(cursor.getColumnIndex(DatabaseHelper.WEATHER_TEMPERATURE_COLUMN));
            int    humidity = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.WEATHER_HUMIDITY_COLUMN));
            double windSpeed = cursor.getDouble(cursor.getColumnIndex(DatabaseHelper.WEATHER_WIND_SPEED_COLUMN));
            double windDirection = cursor.getDouble(cursor.getColumnIndex(DatabaseHelper.WEATHER_WIND_DEGREE_COLUMN));
            int    weatherId = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.WEATHER_WEATHER_ID_COLUMN));
            int    cityId = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.WEATHER_CITY_ID_COLUMN));
            long   unixTime = cursor.getLong(cursor.getColumnIndex(DatabaseHelper.WEATHER_UNIX_TIME_COLUMN));

            weathers.add(new Weather(id, temperature, humidity, windSpeed, windDirection, weatherId, cityId, unixTime));
        }*/


        cursor.close();
    }

}