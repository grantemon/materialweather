package com.grantemon.www.materialweather;

import android.widget.EditText;
import android.widget.TextView;

import com.grantemon.www.materialweather.models.WeatherConsts;

import java.io.IOException;
import java.net.URLEncoder;

public class Load {
    private static final String TAG = "myLogs";


    EditText cityInputForCitySearch;
    TextView cityNameSearchResult;
    TextView cityIdSearchResult;

    public Load(EditText cityInput, TextView cityOutputName,TextView cityOutputId, String param, String request) {
        cityInputForCitySearch = cityInput;
        cityNameSearchResult   = cityOutputName;
        cityIdSearchResult     = cityOutputId;

        switch (request){
            case WeatherConsts.FIND_REQUEST:
                //for correct city searching
                StringBuilder correctCityName = new StringBuilder(param);
                correctCityName.append(",");
                new FindCityAsyncTask(cityNameSearchResult,cityIdSearchResult).execute(correctCityName.toString());
                break;
            case WeatherConsts.CURRENT_WEATHER_REQUEST:

                break;
            case WeatherConsts.FIVE_DAY_WEATHER_REQUEST:

                break;
        }

    }

    static public String requestExecute(String request, String param) throws IOException {
        String requestParameter = URLEncoder.encode(param, "UTF-8");

        String requestStringInJson;
        StringBuilder requestStringBulder = new StringBuilder();

        requestStringBulder.append(request);
        requestStringBulder.append(requestParameter);
        requestStringBulder.append(WeatherConsts.API_KEY);

        requestStringInJson = ServiceHandler.makeServiceCall(requestStringBulder.toString());

        return requestStringInJson;
    }

}
