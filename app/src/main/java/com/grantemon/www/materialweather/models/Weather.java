package com.grantemon.www.materialweather.models;

public class Weather {
    private int    id;
    private double temperature;
    private int    humidity;
    private double windSpeed;
    private double windDirection;
    private int    weatherId;
    private int    cityId;
    private long   unixTime;

    public Weather(int id, double temperature, int humidity, double windSpeed, double windDirection, int weatherId, int cityId, long unixTime) {
        this.id = id;
        this.temperature = temperature;
        this.humidity = humidity;
        this.windSpeed = windSpeed;
        this.windDirection = windDirection;
        this.weatherId = weatherId;
        this.cityId = cityId;
        this.unixTime = unixTime;
    }

    public Weather() {
        this.id = 1;
        this.temperature = 0;
        this.humidity = 0;
        this.windSpeed = 0;
        this.windDirection = 0;
        this.weatherId = 0;
        this.cityId = 0;
        this.unixTime = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public int getHumidity() {
        return humidity;
    }

    public void setHumidity(int humidity) {
        this.humidity = humidity;
    }

    public double getWindSpeed() {
        return windSpeed;
    }

    public void setWindSpeed(double windSpeed) {
        this.windSpeed = windSpeed;
    }

    public double getWindDirection() {
        return windDirection;
    }

    public void setWindDirection(double windDirection) {
        this.windDirection = windDirection;
    }

    public int getWeatherId() {
        return weatherId;
    }

    public void setWeatherId(int weatherId) {
        this.weatherId = weatherId;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    public long getUnixTime() {
        return unixTime;
    }

    public void setUnixTime(long unixTime) {
        this.unixTime = unixTime;
    }
}
