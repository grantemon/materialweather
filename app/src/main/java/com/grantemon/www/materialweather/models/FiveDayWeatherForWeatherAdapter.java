package com.grantemon.www.materialweather.models;

public class FiveDayWeatherForWeatherAdapter {
    String day;
    int weatherId;
    int temperatureMin ;
    int temperatureMax;

    public FiveDayWeatherForWeatherAdapter(String day, int weatherId, int temperatureMin, int temperatureMax) {
        this.day = day;
        this.weatherId = weatherId;
        this.temperatureMin = temperatureMin;
        this.temperatureMax = temperatureMax;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public int getWeatherId() {
        return weatherId;
    }

    public void setWeatherId(int weatherId) {
        this.weatherId = weatherId;
    }

    public int getTemperatureMin() {
        return temperatureMin;
    }

    public void setTemperatureMin(int temperatureMin) {
        this.temperatureMin = temperatureMin;
    }

    public int getTemperatureMax() {
        return temperatureMax;
    }

    public void setTemperatureMax(int temperatureMax) {
        this.temperatureMax = temperatureMax;
    }
}

