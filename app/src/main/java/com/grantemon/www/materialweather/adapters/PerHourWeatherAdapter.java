package com.grantemon.www.materialweather.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.grantemon.www.materialweather.R;
import com.grantemon.www.materialweather.models.Weather;
import com.grantemon.www.materialweather.models.WeatherConsts;

import java.util.ArrayList;
import java.util.Date;

public class PerHourWeatherAdapter extends BaseAdapter {
    Context ctx;
    LayoutInflater lInflater;
    ArrayList<Weather> objects;

    public PerHourWeatherAdapter(Context context, ArrayList<Weather> weathers) {
        ctx = context;
        objects = weathers;
        lInflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public Object getItem(int position) {
        return objects.get(position);
    }


    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = lInflater.inflate(R.layout.per_hour_weather_list_item, parent, false);
        }

        Weather weather = getWeather(position);
        Date dt = new Date(weather.getUnixTime()*1000);
        StringBuilder date = new StringBuilder();
        date.append(dt.getHours());
        date.append(":00");


        //Log.v(WeatherConsts.TAG, String.valueOf(weather.getTemperature()));
        //Log.v(WeatherConsts.TAG, String.valueOf(WeatherConsts.getCelsiusTemperature(weather.getTemperature())));




        ((TextView) view.findViewById(R.id.perHourWeatherItemTime)).setText(date);
        ((TextView)view.findViewById(R.id.perHourWeatherItemWeatherName)).setText(String.valueOf(WeatherConsts.getCelsiusTemperature(weather.getTemperature())));
        ((TextView)view.findViewById(R.id.perHourWeatherItemTemperature)).setText(WeatherConsts.getWeatherById(weather.getWeatherId()));

        return view;
    }

    Weather getWeather(int position) {
        return ((Weather) getItem(position));
    }

}
