package com.grantemon.www.materialweather.models;

import android.content.Context;
import android.net.ConnectivityManager;

import java.util.HashMap;
import java.util.Map;

public class WeatherConsts {
    public static final Map<Integer, String> WEATHER_IDS = new HashMap<>();
    public static final String FIND_REQUEST              = "http://api.openweathermap.org/data/2.5/find?q=";;
    public static final String FIVE_DAY_WEATHER_REQUEST  = "http://api.openweathermap.org/data/2.5/forecast?q=";
    public static final String CURRENT_WEATHER_REQUEST   = "http://api.openweathermap.org/data/2.5/weather?q=";
    public static final String API_KEY                   = "&APPID=014740491dd81522243503dde2d26436";
    public static final String EMPTY_CITY_NAME           = "No such city";
    public static final String EMPTY_STRING              = "";
    public static final int ZERO                         = 0;
    public static final int FIRST                        = 1;
    public static final int EMPTY_CITY_ID                = 41;
    public static final int CITY_NAME                    = 1;
    public static final int EMPTY_ID                     = 0;
    public static final int TRUE                         = 1;
    public static final int FALSE                        = 0;
    public static final int CURRENT_WEATHER_POSITION     = 1;
    public static final int FIVE_DAY_WEATHER_START       = 2;
    public static final int FIVE_DAY_WEATHER_END         = 41;
    public static final int PAGES_COUNT                  = 4;
    public static final String PREFERANCES_ACTIVE_CITY   = "activeCity";
    public static final String PREFERENCES_FILE          = "activeCityPreferences";
    public static final String TAG                       = "myLogs";

    static {
        WEATHER_IDS.put(200, "гроза со слабым дождём");
        WEATHER_IDS.put(201, "гроза с дождём");
        WEATHER_IDS.put(202, "гроза с проливным дождём");
        WEATHER_IDS.put(210, "слабая гроза");
        WEATHER_IDS.put(211, "гроза");
        WEATHER_IDS.put(212, "сильная гроза");
        WEATHER_IDS.put(221, "жестокая гроза");
        WEATHER_IDS.put(230, "гроза со слабым моросящим дождём");
        WEATHER_IDS.put(231, "гроза с моросящим дождём");
        WEATHER_IDS.put(232, "гроза с сильным моросящим дождём");
        WEATHER_IDS.put(300, "слабая морось");
        WEATHER_IDS.put(301, "морось");
        WEATHER_IDS.put(302, "сильная морось");
        WEATHER_IDS.put(310, "слабый моросящий дождь");
        WEATHER_IDS.put(311, "моросящий дождь");
        WEATHER_IDS.put(312, "сильный моросящий дождь");
        WEATHER_IDS.put(313, "ливень и изморось");
        WEATHER_IDS.put(314, "сильный ливень и изморось");
        WEATHER_IDS.put(321, "проливной моросящий дождь");
        WEATHER_IDS.put(500, "слабый дождь");
        WEATHER_IDS.put(501, "дождь");
        WEATHER_IDS.put(502, "сильный дождь");
        WEATHER_IDS.put(503, "очень сильный дождь");
        WEATHER_IDS.put(504, "очень сильный дождь");
        WEATHER_IDS.put(511, "ледяной дождь");
        WEATHER_IDS.put(520, "слабый проливной дождь");
        WEATHER_IDS.put(521, "проливной дождь");
        WEATHER_IDS.put(522, "сильный проливной дождь");
        WEATHER_IDS.put(531, "сильный ливень");
        WEATHER_IDS.put(600, "небольшой снегопад");
        WEATHER_IDS.put(601, "снегопад");
        WEATHER_IDS.put(602, "сильный снегопад");
        WEATHER_IDS.put(611, "слякоть");
        WEATHER_IDS.put(612, "дождь и слякоть");
        WEATHER_IDS.put(615, "слабый дождь и снег");
        WEATHER_IDS.put(616, "дождь и снег");
        WEATHER_IDS.put(620, "слабый мокрый снег");
        WEATHER_IDS.put(621, "снегопад");
        WEATHER_IDS.put(622, "сильный мокрый снег");
        WEATHER_IDS.put(701, "туман");
        WEATHER_IDS.put(701, "дымка");
        WEATHER_IDS.put(711, "смог");
        WEATHER_IDS.put(721, "дымка");
        WEATHER_IDS.put(731, "песчаная буря");
        WEATHER_IDS.put(741, "туман");
        WEATHER_IDS.put(751, "песок");
        WEATHER_IDS.put(761, "пыль");
        WEATHER_IDS.put(752, "ВУЛКАНИЧЕСКИЙ ПЕПЕЛ");
        WEATHER_IDS.put(771, "ШКВАЛ");
        WEATHER_IDS.put(781, "ТОРНАДО");
        WEATHER_IDS.put(800, "ясно");
        WEATHER_IDS.put(801, "слегка облачно");
        WEATHER_IDS.put(802, "облачно");
        WEATHER_IDS.put(803, "пасмурно с прояснениями");
        WEATHER_IDS.put(804, "пасмурно");
        WEATHER_IDS.put(900, "торнадо");
        WEATHER_IDS.put(901, "тропический шторм");
        WEATHER_IDS.put(902, "ураган");
        WEATHER_IDS.put(903, "мороз");
        WEATHER_IDS.put(904, "жара");
        WEATHER_IDS.put(905, "ветренно");
        WEATHER_IDS.put(906, "град");
        WEATHER_IDS.put(951, "Штиль");
        WEATHER_IDS.put(952, "Тихий ветер");
        WEATHER_IDS.put(953, "Легкий ветер");
        WEATHER_IDS.put(954, "Слабый ветер");
        WEATHER_IDS.put(955, "Умеренный ветер");
        WEATHER_IDS.put(956, "Свежий ветер");
        WEATHER_IDS.put(957, "Сильный ветер");
        WEATHER_IDS.put(958, "Крепкий ветер");
        WEATHER_IDS.put(959, "Очень крепкий");
        WEATHER_IDS.put(960, "Шторм");
        WEATHER_IDS.put(961, "Сильный шторм");
        WEATHER_IDS.put(962, "Жестокий шторм");
    }

    public static String getWeatherById(int key){
        return WEATHER_IDS.get(key);
    }
    public static boolean checkInternetConnection(Context context) {
        ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    public static int getCelsiusTemperature(double temp) {
        return (int) Math.round(temp - 273.15);
    }


}