package com.grantemon.www.materialweather;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.view.View;
import android.widget.TextView;

import com.grantemon.www.materialweather.models.Weather;
import com.grantemon.www.materialweather.models.WeatherConsts;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Date;

import static com.grantemon.www.materialweather.Load.requestExecute;

public class LoadCurrentAsyncTask extends AsyncTask<Void,Void, Weather>{
    Context context;
    int activeCity;
    SQLiteDatabase sdb;

    String cityName;
    View currentWeatherPage;


    public LoadCurrentAsyncTask(Context context) {
        this.context = context;
    }

    public LoadCurrentAsyncTask(Context context, View cityPage) {
        this.context = context;
        this.currentWeatherPage = cityPage;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        SharedPreferences sPref;
        sPref = context.getSharedPreferences(WeatherConsts.PREFERENCES_FILE, Context.MODE_PRIVATE);
        activeCity = sPref.getInt(WeatherConsts.PREFERANCES_ACTIVE_CITY, 0);

        DatabaseHelper mDatabaseHelper = new DatabaseHelper(context,DatabaseHelper.DATABASE_NAME, null, DatabaseHelper.DATABASE_VERSION);
        sdb = mDatabaseHelper.getReadableDatabase();
    }

    @Override
    protected Weather doInBackground(Void... params) {

        Weather weather = new Weather();

        Cursor cursor = sdb.query(DatabaseHelper.CITIES_TABLE, new String[]{DatabaseHelper.ID_COLUMN,
                        DatabaseHelper.CITY_ID_COLUMN,DatabaseHelper.CITY_NAME_COLUMN,DatabaseHelper.ACTIVE},
                null, null,
                null, null, null) ;

        cursor.move(activeCity);
        cityName = cursor.getString(cursor.getColumnIndex(DatabaseHelper.CITY_NAME_COLUMN));

        String result;
        try {
            result = requestExecute(WeatherConsts.CURRENT_WEATHER_REQUEST, cityName);
            weather = currentWeather(result);
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        cursor.close();
        return weather;
    }

    @Override
    protected void onPostExecute(Weather weather) {
        super.onPostExecute(weather);

        ContentValues values = new ContentValues();

        values.put(DatabaseHelper.WEATHER_TEMPERATURE_COLUMN, weather.getTemperature());
        values.put(DatabaseHelper.WEATHER_HUMIDITY_COLUMN   , weather.getHumidity());
        values.put(DatabaseHelper.WEATHER_WIND_SPEED_COLUMN, weather.getWindSpeed());
        values.put(DatabaseHelper.WEATHER_WIND_DEGREE_COLUMN, weather.getWindDirection());
        values.put(DatabaseHelper.WEATHER_WEATHER_ID_COLUMN, weather.getWeatherId());
        values.put(DatabaseHelper.WEATHER_CITY_ID_COLUMN, weather.getCityId());
        values.put(DatabaseHelper.WEATHER_UNIX_TIME_COLUMN, weather.getUnixTime());
        sdb.update(DatabaseHelper.WEATHERS_TABLE, values, "id = ?", new String[]{
                String.valueOf(WeatherConsts.CURRENT_WEATHER_POSITION)
        });

        TextView cityName                    = (TextView) currentWeatherPage.findViewById(R.id.cityName);
        TextView currentWeatherTemperature   = (TextView) currentWeatherPage.findViewById(R.id.currentWeatherTemperature);
        TextView currentWeatherName          = (TextView) currentWeatherPage.findViewById(R.id.currentWeatherName);
        TextView currentWeatherHumidity      = (TextView) currentWeatherPage.findViewById(R.id.currentWeatherHumidity);
        TextView currentWeatherWindSpeed     = (TextView) currentWeatherPage.findViewById(R.id.currentWeatherWindSpeed);
        TextView currentWeatherWindDirection = (TextView) currentWeatherPage.findViewById(R.id.currentWeatherWindDirection);

        cityName.setText(String.format("%s %s", this.cityName, new Date()));
        currentWeatherTemperature.setText(String.valueOf(WeatherConsts.getCelsiusTemperature(weather.getTemperature())) + " C");
        currentWeatherName.setText(WeatherConsts.getWeatherById(weather.getWeatherId()));
        currentWeatherHumidity.setText("Влажность " + String.valueOf(weather.getHumidity()));
        currentWeatherWindSpeed.setText("Скорость ветра " + String.valueOf(weather.getWindSpeed()));
        currentWeatherWindDirection.setText("Направление ветра " + String.valueOf(weather.getWindDirection()));

    }

    private Weather currentWeather(String currentWeatherInformation) throws JSONException, IOException {

        Weather weather = null;

        JSONObject jsonObjectWithWeatherInformation;
        JSONObject jsonObjectWithWeatherSpecifications;
        JSONArray arrayWithWeatherInformation;

        double temperature;
        int    humidity;
        double windSpeed;
        double windDegree;
        int    weatherId;
        int    cityId;
        long   unixTime;

        if (currentWeatherInformation != null) {
            JSONObject currentWeatherRequestJsonObj = new JSONObject(currentWeatherInformation);

            arrayWithWeatherInformation      = currentWeatherRequestJsonObj.getJSONArray("weather");
            jsonObjectWithWeatherInformation = arrayWithWeatherInformation.getJSONObject(0);

            jsonObjectWithWeatherSpecifications = currentWeatherRequestJsonObj.getJSONObject("main");
            temperature  = jsonObjectWithWeatherSpecifications.getDouble("temp");
            humidity = jsonObjectWithWeatherSpecifications.getInt("humidity");

            jsonObjectWithWeatherSpecifications = currentWeatherRequestJsonObj.getJSONObject("wind");
            windSpeed  = jsonObjectWithWeatherSpecifications.getDouble("speed");
            windDegree = jsonObjectWithWeatherSpecifications.getDouble("deg");
            weatherId  = jsonObjectWithWeatherInformation.getInt("id");
            cityId     = currentWeatherRequestJsonObj.getInt("id");
            unixTime   = currentWeatherRequestJsonObj.getLong("dt");

            weather = new Weather(WeatherConsts.CURRENT_WEATHER_POSITION,
                    temperature,humidity,windSpeed,windDegree,weatherId,cityId,unixTime);
        }
        return weather;
    }
}
