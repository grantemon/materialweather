package com.grantemon.www.materialweather;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;

import com.grantemon.www.materialweather.models.WeatherConsts;

public class MainActivity extends FragmentActivity {

    ViewPager pager;
    PagerAdapter pagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /*SharedPreferences sPref;
        sPref = getSharedPreferences(WeatherConsts.PREFERENCES_FILE, MODE_PRIVATE);
        SharedPreferences.Editor ed = sPref.edit();
        ed.putString(WeatherConsts.PREFERANCES_ACTIVE_CITY, String.valueOf(1));
        ed.commit();
        Log.v(WeatherConsts.TAG, "adeed sh pr");
        Log.v(WeatherConsts.TAG, "before loading");
        new LoadCurrentAsyncTask(this);
        Log.v(WeatherConsts.TAG, "after loading loading");*/

        SharedPreferences sPref;
        sPref = getSharedPreferences(WeatherConsts.PREFERENCES_FILE, MODE_PRIVATE);
        SharedPreferences.Editor editor = sPref.edit();
        editor.putInt(WeatherConsts.PREFERANCES_ACTIVE_CITY, 4);
        editor.apply();

        /*if (checkInternetConnection(this)){
            new LoadCurrentAsyncTask(this).execute();
        }*/



        pager = (ViewPager) findViewById(R.id.pager);
        pagerAdapter = new MyFragmentPagerAdapter(getSupportFragmentManager());
        pager.setAdapter(pagerAdapter);
        PageFragment.setContext(this);


        pager.setOnPageChangeListener(new OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
            }

            @Override
            public void onPageScrolled(int position, float positionOffset,
                                       int positionOffsetPixels) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    private class MyFragmentPagerAdapter extends FragmentPagerAdapter {

        public MyFragmentPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            return PageFragment.newInstance(position);
        }

        @Override
        public int getCount() {
            return WeatherConsts.PAGES_COUNT;
        }
    }

    public final boolean checkInternetConnection(Context context) {
        ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }
}