package com.grantemon.www.materialweather;

import android.content.ContentValues;
import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.grantemon.www.materialweather.models.WeatherConsts;

import static com.grantemon.www.materialweather.models.WeatherConsts.TAG;

public class DatabaseHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "weatherdb.db";
    public static final int DATABASE_VERSION = 2;

    public static final String CITIES_TABLE  = "cities";
    public static final String WEATHERS_TABLE = "weathers";

    public static final String ID_COLUMN        = "id";
    public static final String CITY_ID_COLUMN   = "city_id";
    public static final String CITY_NAME_COLUMN = "name";
    public static final String ACTIVE           = "active";

    public static final String WEATHER_ID_COLUMN          = "id";
    public static final String WEATHER_TEMPERATURE_COLUMN = "temperature";
    public static final String WEATHER_HUMIDITY_COLUMN    = "humidity";
    public static final String WEATHER_WIND_SPEED_COLUMN  = "wind_speed";
    public static final String WEATHER_WIND_DEGREE_COLUMN = "wind_degree";
    public static final String WEATHER_WEATHER_ID_COLUMN  = "weather_id";
    public static final String WEATHER_CITY_ID_COLUMN     = "city_id";
    public static final String WEATHER_UNIX_TIME_COLUMN   = "unix_time";


    private static final String CITIES_TABLE_CREATION_SCRIPT = "create table "
            + CITIES_TABLE     + " ("
            + ID_COLUMN        + " integer primary key autoincrement, "
            + CITY_ID_COLUMN   + " integer, "
            + ACTIVE           + " integer, "
            + CITY_NAME_COLUMN + " text not null);";

    private static final String WEATHERS_TABLE_CREATION_SCRIPT = "create table "
            + WEATHERS_TABLE             + " ("
            + WEATHER_ID_COLUMN          + " integer primary key autoincrement, "
            + WEATHER_TEMPERATURE_COLUMN + " real, "
            + WEATHER_HUMIDITY_COLUMN    + " integer, "
            + WEATHER_WIND_SPEED_COLUMN  + " real, "
            + WEATHER_WIND_DEGREE_COLUMN + " real, "
            + WEATHER_WEATHER_ID_COLUMN  + " integer, "
            + WEATHER_CITY_ID_COLUMN     + " integer, "
            + WEATHER_UNIX_TIME_COLUMN   + " integer);";


    private static final String CITIES_TABLE_DELITION_SCRIPT   =  "DROP TABLE IF EXISTS '" + CITIES_TABLE   + "'" + ";";
    private static final String WEATHERS_TABLE_DELITION_SCRIPT =  "DROP TABLE IF EXISTS '" + WEATHERS_TABLE + "'" + ";";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public DatabaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public DatabaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version, DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CITIES_TABLE_CREATION_SCRIPT);
        db.execSQL(WEATHERS_TABLE_CREATION_SCRIPT);



        ContentValues emptyWeatherValues = new ContentValues();
        emptyWeatherValues.put(WEATHER_TEMPERATURE_COLUMN,WeatherConsts.ZERO);
        emptyWeatherValues.put(WEATHER_HUMIDITY_COLUMN,WeatherConsts.ZERO);
        emptyWeatherValues.put(WEATHER_WIND_SPEED_COLUMN,WeatherConsts.ZERO);
        emptyWeatherValues.put(WEATHER_WIND_DEGREE_COLUMN,WeatherConsts.ZERO);
        emptyWeatherValues.put(WEATHER_WEATHER_ID_COLUMN,WeatherConsts.ZERO);
        emptyWeatherValues.put(WEATHER_CITY_ID_COLUMN,WeatherConsts.ZERO);
        emptyWeatherValues.put(WEATHER_UNIX_TIME_COLUMN,WeatherConsts.ZERO);


        for (int i = 0; i < WeatherConsts.FIVE_DAY_WEATHER_END; i++) {
            db.insert(WEATHERS_TABLE, WEATHER_TEMPERATURE_COLUMN, emptyWeatherValues);
            db.insert(WEATHERS_TABLE, WEATHER_HUMIDITY_COLUMN, emptyWeatherValues);
            db.insert(WEATHERS_TABLE, WEATHER_WIND_SPEED_COLUMN, emptyWeatherValues);
            db.insert(WEATHERS_TABLE, WEATHER_WIND_DEGREE_COLUMN, emptyWeatherValues);
            db.insert(WEATHERS_TABLE, WEATHER_WEATHER_ID_COLUMN, emptyWeatherValues);
            db.insert(WEATHERS_TABLE, WEATHER_CITY_ID_COLUMN, emptyWeatherValues);
            db.insert(WEATHERS_TABLE, WEATHER_UNIX_TIME_COLUMN, emptyWeatherValues);
        }

        Log.d(TAG, "inserted in weathers");

        ContentValues values = new ContentValues();
        values.put(CITY_ID_COLUMN, 0);
        values.put(CITY_NAME_COLUMN, "");
        values.put(ACTIVE, 0);
        for (int i = 0; i < 4; i++) {
            db.insert(CITIES_TABLE, CITY_ID_COLUMN, values);
            db.insert(CITIES_TABLE, CITY_NAME_COLUMN, values);
            db.insert(CITIES_TABLE, ACTIVE, values);
        }
        Log.d(TAG, "inserted in cities");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d(TAG, "upgrade 4 " + oldVersion + ":" + newVersion);

        db.execSQL(CITIES_TABLE_DELITION_SCRIPT);
        db.execSQL(WEATHERS_TABLE_DELITION_SCRIPT);
        Log.d(TAG, "upgraded");

        onCreate(db);
    }
}
